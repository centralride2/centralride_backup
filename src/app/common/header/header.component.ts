import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {UserService} from '../services/user/user.service';
import {HttpService} from '../services/http/http.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {

  userDetails: object = {};
  profileImage: string = '';

  constructor(private router: Router, private users: UserService,public http:HttpService,
    private spinner: NgxSpinnerService) {
  }

  loggedIn: boolean = false;

  ngOnInit() {
      this.getUserInfo();    
  }

  getUserInfo() {
    this.spinner.show();
    this.userDetails = this.users.currentUserValue; 
    setTimeout(() => {
      this.spinner.hide();
    }, 500);
    if (this.userDetails) {
      this.loggedIn = true;
      if (this.userDetails['profile_image']) {
        this.profileImage = this.http.getImageLink() + this.userDetails['profile_image'];
        // this.profileImage = 'https://shuttle.netsolutionindia.com/media/' + this.userDetails['profile_image'];
      }
    }
  }

  logout() {
    this.loggedIn = false;

    this.users.userSignOut();
    window.location.reload();
  }

}
