import {Component, OnInit, ElementRef} from '@angular/core';
import {Router} from '@angular/router';
import {ApiUrl} from '../core/apiUrl';
import {HttpService} from '../services/http/http.service';
import {MessageService} from '../services/message/message.service';
declare var $: any;

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html'
})
export class FooterComponent implements OnInit {

  mapLinkCode: any;

  constructor(private http: HttpService, private router: Router, private hostElement: ElementRef, public message: MessageService) {
  }

  ngOnInit() {
    this.mapLink();
    if (this.mapLinkCode) {

    }

  }

  email: '';

  forgotPass() {
    if (this.email) {
      $('#forget-password').modal('hide');
      this.http.postData(ApiUrl.forgotpassword, {email: this.email}, true).subscribe(response => {
        console.log(response,'response');
        if (response.statuscode === 200) {
          this.http.openSnackBar(response.msg, true);
          window.location.reload();
          // this.message.showNotification('success', response.msg);
        } else {
          this.http.openSnackBar(response.msg, true);
          // this.message.showNotification('danger', response.msg);

        }
        this.email = '';
      }, error => {
        $('#forget-password').modal('hide');
        this.email = '';
        this.http.openSnackBar(error.error.msg, true);
        window.location.reload();
        // this.message.showNotification('danger', error.error.msg);
      });
    } else {
      this.http.openSnackBar('Please enter email' , true);
      // this.message.showNotification('danger', 'Please enter email');
    }
  }

  mapLink() {

    this.http.getData(ApiUrl.contactUs, {}).subscribe(response => {
      if (response.data) {
        // console.log('response.dataresponse.data', response.data);
        // console.log('response.dataresponse.data', response.data.contact_details['maps_embed']);
        this.mapLinkCode = response.data.contact_details;

        const iframe = this.hostElement.nativeElement.querySelector('iframe');
        // console.log('iframeiframe', iframe);
        // console.log('iframeiframe>>>>>>', this.mapLinkCode['maps_embed']);

        iframe.src = this.mapLinkCode['maps_embed'];

      } else {
      }
    }, error => {
    });
  }

}
