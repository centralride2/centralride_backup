import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable, Inject } from '@angular/core';
import { Router } from '@angular/router';
import {HttpService} from '../http/http.service'
@Injectable({
  providedIn: 'root'
})
export class UserService {

  private currentUserSubject: BehaviorSubject<any>;
  private currentLoginUserSubject: BehaviorSubject<any>;
  private currentRegisterUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;

  constructor(
    // @Inject(LOCAL_STORAGE) private localStorage: any,
    private router: Router,private http:HttpService) {

    /********* Check JSON parse error on fetching currentUser from local storage **********/
    let _user: any = null;
    let _registered: any = null;
    let _loginData: any = null;
    try {
      _user = JSON.parse(localStorage.getItem('web_user'));
    } catch (error) {
      if (error instanceof SyntaxError) this.removeUser();
    }
    this.currentUserSubject = new BehaviorSubject<any>(_user);
    this.currentUser = this.currentUserSubject.asObservable();

    this.currentLoginUserSubject = new BehaviorSubject<any>(_loginData);
    try {
      _registered = JSON.parse(localStorage.getItem('register_user'));
    } catch (error) {
      if (error instanceof SyntaxError) this.removeUser();
    }
    this.currentRegisterUserSubject = new BehaviorSubject<any>(_registered);
  }

  /********* Get the current value of the logged in user **********/
  public get currentUserValue(): any {
    
    return this.currentUserSubject.value;
  }

  /********* Get the current value of the registered user **********/
  public get currentRegisterUserValue(): any {
    return this.currentRegisterUserSubject.value;
  }

  /********* Get the current user token **********/
  public get getUserToken(): string {
    if (!!this.currentUserValue) {
      return this.currentUserValue.token;
    }
  }

  /********* Get the current registration token **********/
  public get getRegisterationData(): any {
    return this.currentLoginUserSubject.value;
  }

  /********* Set user in local storage **********/
  setUserLocalData(userData: any) {
    this.removeRegister()
    localStorage.setItem('web_user', JSON.stringify(userData));
    this.currentUserSubject.next(userData);
  }

  /********* Set registeration data in local storage **********/
  setRegisterationLocalData(userData: any) {
    this.removeRegister()
    localStorage.setItem('register_user', JSON.stringify(userData));
    this.currentRegisterUserSubject.next(userData);
  }

  setUserRegisterData(registerData: any) {
    this.removeRegisterData()
    localStorage.setItem('user_register_data', JSON.stringify(registerData));
    this.currentLoginUserSubject.next(registerData);
  }

  /********* Remove user from local storage **********/
  removeUser() {
    //localStorage.removeItem('web_user');
    localStorage.clear();
    if (this.currentUserSubject) {
      this.currentUserSubject.next(null);
    }
  }

  removeRegisterData() {
    localStorage.clear();
    if (this.currentLoginUserSubject) {
      this.currentLoginUserSubject.next(null);
    }
  }

  removeRegister() {
    //localStorage.removeItem('web_user');
    localStorage.clear();
    if (this.currentRegisterUserSubject) {
      this.currentRegisterUserSubject.next(null);
    }
  }

  /********* User Sign-Out **********/
  userSignOut() {
    this.removeUser();
    this.router.navigate(['/hompage']);
    this.http.openSnackBar('you are successfully logout', true);
  }
}
