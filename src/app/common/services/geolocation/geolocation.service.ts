import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GeolocationService {

  public lat;
  public lng;

  constructor() { }

  getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position: Position) => {
        if (position) {
          this.lat = position.coords.latitude;
          this.lng = position.coords.longitude;
          this.setLocation({lng:this.lng, lat:this.lat})
        }
      },
        (error: PositionError) => console.log(error));
    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }


  private key: BehaviorSubject<any> = new BehaviorSubject(null);
  getLocations: Observable<any> = this.key.asObservable();
  setLocation(data: any) {
    this.key.next(data);
  }
}
