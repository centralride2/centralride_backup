import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable, Inject } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private currentSearchRequest: BehaviorSubject<any>;
  public currentSearch: Observable<any>;

  private messageSource = new BehaviorSubject({});
  currentMessage = this.messageSource.asObservable();

  constructor() {
    let _loginData: any = null;
    this.currentSearchRequest = new BehaviorSubject<any>(_loginData);
    this.currentSearch = this.currentSearchRequest.asObservable();
  }

  changeMessage(message: Object) {
    this.messageSource.next(message)
  }

  public get getSearchData(): any {
    console.log("this.currentSearchRequest.value", this.currentSearchRequest.value)
    return localStorage.getItem('searchRequest')
    // return this.currentSearchRequest.value;
  }

  setSearchData(registerData: any) {
    this.removeSearchData()
    localStorage.setItem('searchRequest', JSON.stringify(registerData));
    this.currentSearchRequest.next(registerData);
  }

  removeSearchData() {
    localStorage.removeItem('searchRequest');
    if (this.currentSearchRequest) {
      this.currentSearchRequest.next(null);
    }
  }

}