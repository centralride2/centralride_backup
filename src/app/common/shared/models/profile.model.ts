export interface ImageUrl {
    original: string;
    thumbnail: string;
}

export interface Logo {
    original: string;
    thumbnail: string;
}

export interface CompanyId {
    _id: string;
    logo: Logo;
    name: string;
    address: string;
    website: string;
    bio: string;
    country: string;
    isBlocked: boolean;
    isDeleted: boolean;
    ownerId: string;
    createdAt: Date;
    updatedAt: Date;
    __v: number;
}

export interface ProfileModel {
    _id: string;
    imageUrl: ImageUrl;
    credits: number;
    isOTPVerified: boolean;
    profileType: string;
    isBlocked: boolean;
    isDeleted: boolean;
    email: string;
    userName: string;
    name: string;
    countryCode: string;
    phoneNumber: string;
    fullPhoneNumber: string;
    coordinates: number[];
    createdAt: Date;
    updatedAt: Date;
    OTPcode: string;
    tokenIssuedAt: number;
    deviceToken: string;
    companyId: CompanyId;
    accessToken: string;
}

export interface FavouriteInfoType {
    _id: string;
    imageUrl: ImageUrl;
    name: string;
    salary: number;
    nationality: string;
    age: number;
    serviceId: string;
    maidNeedServiceId?: any;
}

export interface FavouriteModelInfo {
    data: FavouriteInfoType[];
    count: number;
}

export interface FavouriteModel {
    type: string;
    statusCode: number;
    message: string;
    info: FavouriteModelInfo;
}



export interface BusinessModelInfoType {
    _id: string;
    imageUrl: ImageUrl;
    name: string;
}

export interface BusinessModelInfo {
    data: BusinessModelInfoType[];
    count: number;
}

export interface BusinessModel {
    type: string;
    statusCode: number;
    message: string;
    info: BusinessModelInfo;
}


export interface ServicesModelInfoType {
    _id: string;
    name: string;
    imageUrl: ImageUrl;
}

export interface ServicesModelInfo {
    data: ServicesModelInfoType[];
    count: number;
}

export interface ServicesModel {
    type: string;
    statusCode: number;
    message: string;
    info: ServicesModelInfo;
}


export interface ServiceTraitsModelInfoType {
    _id: string;
    name: string;
    isDeleted: boolean;
    isBlocked: boolean;
    imageUrl: ImageUrl;
    serviceId: string;
}

export interface ServiceTraitsModelInfo {
    data: ServiceTraitsModelInfoType[];
    count: number;
}

export interface ServiceTraitsModel {
    type: string;
    statusCode: number;
    message: string;
    info: ServiceTraitsModelInfo;
}


export interface ApplicantDataModel {
    registerWithCompanyId: string;
    imageUrl: ImageUrl;
    name: string;
    serviceId: string;
    religion: string;
    profession: string;
    age: number;
    salary: number;
    phoneNumber: number;
    nationality: string;
    placeOfBirth: string;
    status: string;
    numberOfChildren: string;
    yearOfExperience: string;
    language: string[];
    height: number;
    weight: number;
    note: string;
    serviceTraitIds: string[];
    latitude: number;
    longitude: number;
}

export interface ListOfBusinessModel {
    businessId: string;
    companyId: string;
    maidNeedServiceId: string;
    numberOfWeeks: number;
}


export interface EditProfileModel {
    name: string;
    countryCode: string;
    phoneNumber: string;
    userName: string;
    address: string;
    website: string;
    imageUrl: ImageUrl;
}