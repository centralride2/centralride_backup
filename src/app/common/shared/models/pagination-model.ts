export class PaginationParams {
    limit: number = 20;
    skip: number = 0;
}

export class Paginate {
    count: number = 0;
    page: number = 0;
}