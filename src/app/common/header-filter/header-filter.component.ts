import {Component, OnInit} from '@angular/core';

import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';

import {ApiUrl} from '../core/apiUrl';
import {HttpService} from '../services/http/http.service';
import {UserService} from '../services/user/user.service';
import {MessageService} from '../services/message/message.service';
import * as moment from 'moment';

import {DataService} from '../services/data/data.service';

@Component({
  selector: 'app-header-filter',
  templateUrl: './header-filter.component.html'
})
export class HeaderFilterComponent implements OnInit {

  vendorSearchForm: FormGroup;
  vendorSearchParams: Object = {};
  today = new Date();
  pickupLocation;
  dropoffLocation;
  
  constructor(private formBuild: FormBuilder, private http: HttpService, private users: UserService, private router: Router, private message: MessageService, private data: DataService) {
  }

  ngOnInit() {
    this.mainSearch();
  }

  searchApiParams() {
    this.vendorSearchParams = {
      start: '',
      startlat: '',
      startlong: '',
      end: '',
      endlat: '',
      endlong: '',
      date: '',
      doorservice: '',
      daytime: 3,
      search: '',
      sortbyprice: 1,
      pageno: 1,
      pageoffset: 10
    };
  }

  mainSearch() {
    this.vendorSearchForm = this.formBuild.group({
      'pickupLocation': ['', Validators.required],
      'dropoffLocation': ['', Validators.required],
      'tripDate': ['', Validators.required]
    });
  }

  homeSearch(valid, value): void {
    if (valid) {
      this.getVendors(value);
    }
  }

  onSwap(pickupLocation,dropoffLocation){
    this.dropoffLocation=pickupLocation;
    this.pickupLocation=dropoffLocation;

    var pick_lat = this.vendorSearchParams['startlat'];
    var pick_long = this.vendorSearchParams['startlong'];

    var drop_lat = this.vendorSearchParams['endlat'];
    var drop_long = this.vendorSearchParams['endlong'];
  }

  addressSearch(event: any, field) {

    if (field == 'pickup') {
      this.vendorSearchParams['start'] = event.formatted_address;
      this.vendorSearchParams['startlat'] = event.lat;
      this.vendorSearchParams['startlong'] = event.lng;
    }

    if (field == 'dropoff') {
      this.vendorSearchParams['end'] = event.formatted_address;
      this.vendorSearchParams['endlat'] = event.lat;
      this.vendorSearchParams['endlong'] = event.lng;
    }
  }

  getVendors(value) {
    this.vendorSearchParams['date'] = moment(value.tripDate).format('YYYY-MM-DD');
    console.log('valuevalue', value);
    console.log('valuevalue', this.vendorSearchParams);
    this.data.setSearchData(this.vendorSearchParams);
     // window.location.reload();
    // this.router.navigate(['/vendor-listing']);
  }

}
