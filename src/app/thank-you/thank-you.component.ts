import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
// import { LocationStrategy } from '@angular/common';
// import { PlatformLocation } from '@angular/common';
import {Location} from '@angular/common';

@Component({
  selector: 'app-thank-you',
  templateUrl: './thank-you.component.html'
})
export class ThankYouComponent implements OnInit {

  constructor(private router: Router ,  private location: Location) {
    window.location.hash="no-back-button";
    window.location.hash="Again-No-back-button";//again because google chrome don't insert first hash into history
    window.onhashchange=function(){window.location.hash="no-back-button";}
  }
  ngOnInit() {
  }
  


}