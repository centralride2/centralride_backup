import {Component, OnInit} from '@angular/core';
import {ApiUrl} from '../common/core/apiUrl';
import {HttpService} from '../common/services/http/http.service';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html'
})
export class PrivacyPolicyComponent implements OnInit {

  privacyPolicyHtml: any;

  constructor(private http: HttpService) {
  }

  ngOnInit() {
    this.getPrivacyPolicyPage();
  }

  getPrivacyPolicyPage() {
    let obj = {
      page: 'privacy-policy'
    };
    this.http.postData(ApiUrl.pages, obj).subscribe(response => {
      if (response.data) {
        this.privacyPolicyHtml = response.data.content;
      } else {
        // this.message.showNotification('danger', response.msg);
      }

    }, error => {
      // this.message.showNotification('danger', error.error.msg);
    });
  }

}
