import {Component, OnInit} from '@angular/core';
import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {ApiUrl} from '../common/core/apiUrl';
import {GlobalVariable} from '../common/core/global';
import {HttpService} from '../common/services/http/http.service';
import {UserService} from '../common/services/user/user.service';
import {MessageService} from '../common/services/message/message.service';
import {DataService} from '../common/services/data/data.service';
import {LoaderService} from '../common/services/loader/loader.service';

@Component({
  selector: 'app-vendor-listing',
  templateUrl: './vendor-listing.component.html'
})
export class VendorListingComponent implements OnInit {

    filterForm: FormGroup;
    vendorSearchParams: any;
    vendors: any = [];
    vendorsCount: number = 0;
    servicetype:any;
    servicetype1;
    servicetype2;
    payment_types;
    imageurl = GlobalVariable.baseImagePath;

  
  constructor(private formBuild: FormBuilder, private http: HttpService, private users: UserService, private router: Router, private message: MessageService, private data: DataService , private spinner: LoaderService) {
    this.vendorSearchParams = JSON.parse(this.data.getSearchData);
  }

  ngOnInit() {
    this.filterFormGeneration();
    this.getVendors();
    this.spinner.show();
  }

  getVendors(data = []) {
    for (var key in data) {
      this.vendorSearchParams[key] = data[key];
    }
    this.spinner.show();
    
    this.http.getData(ApiUrl.getvendors, this.vendorSearchParams).subscribe(response => {
      if (response.data) {
        this.vendors = response.data.data;
        this.vendorsCount = response.data.count;
        this.spinner.show();
      } else {
        this.message.showNotification('danger', response.msg);
        this.spinner.hide();
      }

    }, error => {
      this.message.showNotification('danger', error.error.msg);
      this.spinner.hide();
    });
  }

    

    filterFormGeneration() {
        this.filterForm = this.formBuild.group({
            'doorpickup': [''],
            'doordropoff': [''],
            'fixedpickup': [''],
            'fixeddropoff': [''],
            'time_filter1': [''],
            'time_filter2': [''],
            'time_filter3': [''],
            'time_filter4': [''],
            'sortby': [''],
            'cashpayment': [''],
            'cardpayment': ['']
        });
    }
    resetForm(){
        this.filterForm.reset();
        this.getVendors();
    }

  filter($event: any = null) {
   
    var data_array= [];

    if (this.filterForm.value) {
        var time_filter = []
        if (this.filterForm.value && this.filterForm.value.time_filter1) {
            time_filter.push(1);
        }
        if (this.filterForm.value && this.filterForm.value.time_filter2) {
            time_filter.push(2);
        }
        if (this.filterForm.value && this.filterForm.value.time_filter3) {
            time_filter.push(3);
        }
        if (this.filterForm.value && this.filterForm.value.time_filter4) {
            time_filter.push(4);
        }
        var quotedAndCommaSeparated = '';
        if(time_filter.length >0){
            quotedAndCommaSeparated = "[" + time_filter.join(",") + "]";
        }
        data_array['time_filter'] = quotedAndCommaSeparated;
    

        if (this.filterForm.value.sortby) {
            data_array['sortby'] = parseInt(this.filterForm.value.sortby);
        }
        if (this.filterForm.value.doorpickup) {
          data_array['doorpickup'] =1;
        }
        if (this.filterForm.value.doordropoff) {
          data_array['doordropoff'] = 1;
        }
        if (this.filterForm.value.fixedpickup) {
          data_array['fixedpickup'] = 1;
        }
        if (this.filterForm.value.fixeddropoff) {
          data_array['fixeddropoff'] = 1;
        }
        if (this.filterForm.value.cashpayment) {
          data_array['cashpayment'] = 1;
        }
        if (this.filterForm.value.cardpayment) {
          data_array['cardpayment'] = 1;
        }
    }

    this.getVendors(data_array);
  }

}
