import {Component, OnInit} from '@angular/core';

import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';

import {ApiUrl} from '../common/core/apiUrl';
import {HttpService} from '../common/services/http/http.service';
import {UserService} from '../common/services/user/user.service';
import {MessageService} from '../common/services/message/message.service';
import {SearchCountryField, TooltipLabel, CountryISO} from 'ngx-intl-tel-input';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-payment-step2',
  templateUrl: './payment-step2.component.html'
})
export class PaymentStep2Component implements OnInit {

  paramSubscription: Subscription;
  bookingBillingAddress: FormGroup;
  bookingBillingData: Object = {};
  separateDialCode = true;
  SearchCountryField = SearchCountryField;
  TooltipLabel = TooltipLabel;
  CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [CountryISO.UnitedStates, CountryISO.UnitedKingdom];
  userDetails: object = {};
  show: boolean = false;
  bookingId: number = 0;

  constructor(private formBuild: FormBuilder, private http: HttpService, private users: UserService, private router: Router, private message: MessageService, private route: ActivatedRoute) {
    this.paramSubscription = this.route.queryParams.subscribe((params: Params) => {
      this.bookingId = params['bookingId'] ? params['bookingId'] : 0;
    });
  }

  ngOnInit() {
    this.bookingInfoForm();
    this.getUserInfo();
  }

  bookingInfoForm() {
    this.bookingBillingAddress = this.formBuild.group({
      'city': ['', Validators.required],
      'country': ['', [Validators.required]],
      'street': ['', Validators.required],
      'state': ['', [Validators.required]],
      'postal_code': ['', [Validators.required]]
    });

  }

  getUserInfo() {
    this.userDetails = this.users.currentUserValue;
  }

  submit(valid, value) {
    console.log('value', value);
    this.show = true;
    if (valid) {
      this.savebookingBillingAddress(value);
    } else {
      console.log('incomplete');
    }
  }

  savebookingBillingAddress(value) {
    this.bookingBillingData = value;
    this.bookingBillingData['booking_id'] = this.bookingId;

    this.http.postData(ApiUrl.billingAddress, this.bookingBillingData).subscribe(response => {
      if (response.data) {
        this.router.navigate(['/payment-step-3'], {queryParams: {bookingId: this.bookingId}});
      }
    }, error => {

      this.message.showNotification('danger', error.error.msg);
      this.router.navigate(['/payment-step-3'], {queryParams: {bookingId: this.bookingId}});
    });
  }

  backScreen() {
    this.router.navigate(['/payment-step-1'], {queryParams: {bookingId: this.bookingId}});
  }

}
