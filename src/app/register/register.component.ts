import {Component, OnInit } from '@angular/core';

import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';

import {ApiUrl} from '../common/core/apiUrl';
import {HttpService} from '../common/services/http/http.service';
import {UserService} from '../common/services/user/user.service';
import {MessageService} from '../common/services/message/message.service';
import {SearchCountryField, TooltipLabel, CountryISO} from 'ngx-intl-tel-input';
import {LoaderService} from '../common/services/loader/loader.service';

declare var $: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html'
})

export class RegisterComponent implements OnInit  {

  loginForm: FormGroup;
  signUpForm: FormGroup;
  loginType = false;
  signupType = false;
  loginFormData: Object = {};
  registrationData: Object = {};
  separateDialCode = true;
  SearchCountryField = SearchCountryField;
  TooltipLabel = TooltipLabel;
  CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [CountryISO.UnitedStates, CountryISO.UnitedKingdom];
  submitted = false;

  constructor(private formBuild: FormBuilder, private http: HttpService, private users: UserService,
              private router: Router, private message: MessageService,public loader:LoaderService) {
  }

  ngOnInit() {
    this.loginFormGeneration();
    this.signUpFormGeneration();

  
  
  
  }

  loginFormGeneration() {
    this.loginForm = this.formBuild.group({
      'email': ['', [Validators.required , Validators.email]],
      'password': ['', Validators.required]
    });
  
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  // convenience getter for easy access to form fields
  get f1() { return this.signUpForm.controls; }


  // forgetPasswordFormGeneration() {
  //   this.forgetPasswordForm = this.formBuild.group({
  //     'email': ['', [Validators.required, Validators.email]],
  //   });
  // }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPass.value;
    return pass === confirmPass ? null : {notSame: true};
  }

  signUpFormGeneration() {
    this.signUpForm = this.formBuild.group({
      'name': ['', [Validators.required]],
      'email': ['', [Validators.required, Validators.email]],
      'number': ['', [Validators.required]],
      'password': ['', Validators.required],
      'address':['',[]]
    });
  }

  signIn(valid, value , type , forms): void {
    if(type == 1){
      this.loginType = true;
    }
    this.submitted = true;

    if (valid) {
      this.login(value);
    }
  }

// 

  // forgetPass(valid, value): void {
  //   if (valid) {
  //     // this.login(value);
  //   }
  // }

  login(value) {
    this.http.showLoader.emit(true);
    this.loginFormData = value;
    this.http.postData(ApiUrl.login, value).subscribe(response => {
      this.http.showLoader.emit(false);
      if (response.data) { 
        $('#login').modal('hide'); 
        this.http.openSnackBar('you are successfully login', true);
        // this.message.showNotification('success', 'Successfully Logged In!');
        this.users.setUserLocalData(response.data );
        // window.location.reload();
        // this.router.navigate(['/homepage']);
      } else {
        this.message.showNotification('danger', response.msg);
        // this.router.navigate(['/login']);
      }

    }, error => {
       this.http.openSnackBar(error.error.msg, true);
       // window.location.reload();
      // this.message.showNotification('danger', error.error.msg);
    });
  }

  register(valid, value , type): void {
    if(type == 2){
      this.signupType = true;
    }
    this.submitted = true;
    if (valid) {
      this.sendOtp(value);
    }
  }

  sendOtp(value) {
    this.http.showLoader.emit(true);
    this.loader.show()

    this.registrationData = value;
    this.registrationData['calling_code'] = this.registrationData['number']['dialCode'];
    this.registrationData['mobile'] = this.registrationData['number']['number'];
    delete this.registrationData['number'];
    let number = this.registrationData['calling_code'] + '' + this.registrationData['mobile'].replace(' ', '');
    this.http.postData(ApiUrl.sendSms, {contact_number: number}).subscribe(response => {
      console.log(response , "===========");
      this.http.showLoader.emit(false);
      if (response.data) {
        console.log("-------" , response.data)
        $('#login').modal('hide');
        this.users.setUserRegisterData(this.registrationData);
        $('#verifyotp').modal('show');
        setTimeout(() => {
          alert(response.data['otp']);  
        }, 1000);
        this.loader.hide();
      }
    }, error => {
      this.http.openSnackBar(error.error.msg, true);
      // this.message.showNotification('danger', error.error.msg);
      this.loader.hide()
    });
  }

}
