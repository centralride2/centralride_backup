import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AuthGuard} from './common/services/guards/auth.guard';

import {HomepageComponent} from './homepage/homepage.component';
import {TripListingComponent} from './trip-listing/trip-listing.component';
import {TripDetailComponent} from './trip-detail/trip-detail.component';
import {PaymentStep1Component} from './payment-step1/payment-step1.component';
import {PaymentStep2Component} from './payment-step2/payment-step2.component';
import {PaymentStep3Component} from './payment-step3/payment-step3.component';
import {ThankYouComponent} from './thank-you/thank-you.component';
import {ProfileComponent} from './profile/profile.component';
import {ProfileEditComponent} from './profile-edit/profile-edit.component';
import {ChangePasswordComponent} from './change-password/change-password.component';
// import { ProfileSidebarComponent } from './profile-sidebar/profile-sidebar.component';
import {OtpVerificationComponent} from './otp-verification/otp-verification.component';
import {VendorListingComponent} from './vendor-listing/vendor-listing.component';
import {AffiliateComponent} from './affiliate/affiliate.component';
import {ContactUsComponent} from './contact-us/contact-us.component';
import {AboutUsComponent} from './about-us/about-us.component';
import {TncComponent} from './tnc/tnc.component';

import {AddEditComponent} from './blogs/add-edit/add-edit.component';
import {VendorListComponent} from './blogs/my-list/vendor-list.component';

import {MyBookingsComponent} from './bookings/my-bookings/my-bookings.component';
import {UpcomingBookingsComponent} from './bookings/upcoming-bookings/upcoming-bookings.component';
import {PastBookingsComponent} from './bookings/past-bookings/past-bookings.component';
import {CreateBookingComponent} from './bookings/create-booking/create-booking.component';
import {BookingComponent} from './booking/booking.component';
import {VendorDetailComponent} from './blogs/vendor-detail/vendor-detail.component';
import {BlogDetailComponent} from './blogs/blogs-detail/blog-detail.component';
import {BlogListComponent} from './blogs/list/blog-list.component';
import { NotfoundComponent } from './notfound/notfound.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'homepage',
    pathMatch: 'full'
  },
  {path: 'homepage', component: HomepageComponent},
  {path: 'booking', component: BookingComponent},
  {path: 'trip-listing', component: TripListingComponent},
  {path: 'trip-detail', component: TripDetailComponent},
  {path: 'payment-step-1', component: PaymentStep1Component, canActivate: [AuthGuard]},
  {path: 'payment-step-2', component: PaymentStep2Component, canActivate: [AuthGuard]},
  {path: 'payment-step-3', component: PaymentStep3Component, canActivate: [AuthGuard]},
  {path: 'thank-you', component: ThankYouComponent, canActivate: [AuthGuard]},
  {path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
  {path: 'profile-edit', component: ProfileEditComponent, canActivate: [AuthGuard]},
  {path: 'change-password', component: ChangePasswordComponent, canActivate: [AuthGuard]},
  {path: 'otp-verify', component: OtpVerificationComponent},
  {path: 'vendor-listing', component: VendorListingComponent},
  {path: 'affiliate', component: AffiliateComponent},
  {path: 'contact-us', component: ContactUsComponent},
  {path: 'about-us', component: AboutUsComponent},
  {path: 'terms-n-conditions', component: TncComponent},
  {path: 'blogs/addEdit', component: AddEditComponent, canActivate: [AuthGuard]},
  {path: 'booking/my-bookings', component: MyBookingsComponent, canActivate: [AuthGuard]},
  {path: 'booking/upcoming', component: UpcomingBookingsComponent, canActivate: [AuthGuard]},
  {path: 'booking/past', component: PastBookingsComponent, canActivate: [AuthGuard]},
  {path: 'booking/create', component: CreateBookingComponent, canActivate: [AuthGuard]},
  {path: 'blog-detail/:name/:id', component: BlogDetailComponent},
  {path: 'blog-detail', component: BlogDetailComponent},
  {path: 'vendor-detail', component: VendorDetailComponent},
  {path: 'vendor-list', component: VendorListComponent},
  {path: 'blog-list', component: BlogListComponent},
  // { path: '**', component: NotfoundComponent},
  // { path: 'link-expired', component:  NotfoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
