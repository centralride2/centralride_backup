import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {HttpService} from '../common/services/http/http.service';
import {UserService} from '../common/services/user/user.service';
import {MessageService} from '../common/services/message/message.service';

import {DataService} from '../common/services/data/data.service';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit {

  // paramSubscription: Subscription;
  bookingId: any;
  trips: any = [];
  booking: any;
  myAngularxQrCode: string = null;

  constructor(private http: HttpService, private users: UserService, private router: Router,
              private message: MessageService, private data: DataService, private route: ActivatedRoute) {
    // this.paramSubscription = this.route.queryParams.subscribe((params: Params) => {
    //   this.bookingId = params['bookingId'];
    // });
  }

  token: any = '';

  ngOnInit() {
    this.booking = JSON.parse(localStorage.getItem('bookingData'));
    this.myAngularxQrCode = this.booking.qr;
  }
}
