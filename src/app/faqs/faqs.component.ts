import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ApiUrl} from '../common/core/apiUrl';
import {HttpService} from '../common/services/http/http.service';

@Component({
  selector: 'app-faqs',
  templateUrl: './faqs.component.html'
})
export class FaqsComponent implements OnInit {

  faqsHtml: any;

  constructor(private http: HttpService, private router: Router) {
  }

  ngOnInit() {
    this.getFaqsPage();
  }

  getFaqsPage() {
    let obj = {
      page: 'faq'
    };
    this.http.postData(ApiUrl.pages, obj).subscribe(response => {
      if (response.data) {
        this.faqsHtml = response.data.content;
      } else {
        // this.message.showNotification('danger', response.msg);
      }
    }, error => {
      // this.message.showNotification('danger', error.error.msg);
    });
  }

}
