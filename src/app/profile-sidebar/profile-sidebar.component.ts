import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Validators, FormGroup, FormBuilder} from '@angular/forms';

import {UserService} from '../common/services/user/user.service';
import {HttpService} from '../common/services/http/http.service';
import {ApiUrl} from '../common/core/apiUrl';
import {MessageService} from '../common/services/message/message.service';

@Component({
  selector: 'app-profile-sidebar',
  templateUrl: './profile-sidebar.component.html'
})
export class ProfileSidebarComponent implements OnInit {

  profileImage: FormGroup;
  userDetails: any;
  profileData: any;
  browsedImage: any;
  preview: string = '';
  selectedImageName: string = '';
  loggedIn: boolean = false;

  constructor(private formBuild: FormBuilder, private router: Router, private users: UserService, private http: HttpService, private message: MessageService) {
  }

  ngOnInit() {
    this.getUserInfo();
    this.profileImageForm();
  }

  profileImageForm() {
    this.profileImage = this.formBuild.group({
      'image': ['']
    });
  }

  getUserInfo() {
    this.userDetails = this.users.currentUserValue;
    if (this.userDetails.profile_image) {
      this.preview = this.http.getImageLink() + this.userDetails.profile_image;
      // this.preview = 'https://shuttle.netsolutionindia.com/media/' + this.userDetails.profile_image;
    }

  }

  uploadProfilePic() {
    const formData = new FormData();
    formData.append('name', this.userDetails['name']);
    formData.append('email', this.userDetails['email']);
    formData.append('mobile', this.userDetails['mobile']);
    formData.append('calling_code', this.userDetails['calling_code']);
    formData.append('profile_image', this.browsedImage);

    this.http.postData(ApiUrl.editProfile, formData).subscribe(response => {
      if (response.data) {
        this.users.setUserLocalData(response.data);
        this.profileData = response.data;
        if (this.profileData.profile_image) {
          this.preview = this.http.getImageLink() + this.profileData.profile_image;
          // this.preview = 'https://shuttle.netsolutionindia.com/media/' + this.profileData.profile_image;
        }

      } else {
        this.message.showNotification('danger', response.msg);
      }

    }, error => {
      // this.message.showNotification('danger', error.error.msg);
    });
  }

  onImageSelect(event: any) {
    if (event.target.files && event.target.files[0]) {
      let file = event.target.files[0];
      console.log('event.target.files', event.target.files);
      this.browsedImage = event.target.files[0];

      let reader: FileReader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (e: any) => {
        this.preview = e.target.result;
        this.selectedImageName = file.name;
        console.log('this.browsedImage', this.browsedImage);
        this.uploadProfilePic();
      };
    }
  }

  logout() {
    this.loggedIn = false;
    this.users.userSignOut();
    this.router.navigate(['/homepage']);
  }

}
