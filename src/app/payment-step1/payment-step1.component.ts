import {Component, OnInit} from '@angular/core';

import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';

import {ApiUrl} from '../common/core/apiUrl';
import {HttpService} from '../common/services/http/http.service';
import {UserService} from '../common/services/user/user.service';
import {MessageService} from '../common/services/message/message.service';
import {SearchCountryField, TooltipLabel, CountryISO} from 'ngx-intl-tel-input';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-payment-step1',
  templateUrl: './payment-step1.component.html'
})
export class PaymentStep1Component implements OnInit {

  paramSubscription: Subscription;
  bookingPersonalInfo: FormGroup;
  bookingInfoData: Object = {};
  separateDialCode = true;
  SearchCountryField = SearchCountryField;
  TooltipLabel = TooltipLabel;
  // CountryISO = CountryISO;
  CountryISO: CountryISO | string = CountryISO.UnitedStates;
  preferredCountries: CountryISO[] = [CountryISO.UnitedStates, CountryISO.UnitedKingdom];
  userDetails: object = {};
  show: boolean = false;
  bookingId: number = 0;

  constructor(private formBuild: FormBuilder, private http: HttpService, private users: UserService, private router: Router, private message: MessageService, private route: ActivatedRoute) {
    this.paramSubscription = this.route.queryParams.subscribe((params: Params) => {
      this.bookingId = params['bookingId'] ? params['bookingId'] : 0;
    });
  }

  ngOnInit() {
    this.bookingInfoForm();
    this.getUserInfo();
  }

  bookingInfoForm() {
    this.bookingPersonalInfo = this.formBuild.group({
      'name': ['', Validators.required],
      'email': ['', [Validators.required, Validators.email]],
      'number': ['', Validators.required],
      'alternative_email': ['', [Validators.email]],
      'alt_number': ['', []]
    });
  }

  getUserInfo() {
    this.userDetails = this.users.currentUserValue;

    if (this.userDetails) {
      let obj = {
        name: this.userDetails['name'],
        email: this.userDetails['email'],
        number: this.userDetails['mobile'],
        alternative_email: '',
        alt_number: ''
      };
      // this.selectedCountryISO = 'eu';
      this.CountryISO = ('US').toLowerCase();
      this.bookingPersonalInfo.patchValue(obj);
      // this.bookingPersonalInfo.controls.number.patchValue({ dialCode: this.userDetails["calling_code"], number: this.userDetails["mobile"] })
      // this.bookingPersonalInfo.controls.number.patchValue(this.userDetails["number"])
      // number: {
      //   dialCode: this.userDetails["dialCode"],
      //   number: this.userDetails["number"]
      // },
    }
  }

  submit(valid, value) {
    console.log('value', value);
    this.show = true;
    if (valid) {
      this.saveBookingPersonalInfo(value);
    } else {
      console.log('incomplete');
    }
  }

  saveBookingPersonalInfo(value) {
    this.bookingInfoData = value;

    this.bookingInfoData['phone_code'] = this.bookingInfoData['number']['dialCode'];
    this.bookingInfoData['phone'] = this.bookingInfoData['number']['number'];
    if (this.bookingInfoData['alt_number']) {
      this.bookingInfoData['alternative_phone_code'] = this.bookingInfoData['alt_number']['dialCode'];
      this.bookingInfoData['alternative_phone'] = this.bookingInfoData['alt_number']['number'];
    }

    this.bookingInfoData['booking_id'] = this.bookingId;
    if (!this.bookingInfoData['alternative_email']) {
      delete this.bookingInfoData['alternative_email'];
    }
    delete this.bookingInfoData['alt_number'];
    delete this.bookingInfoData['number'];

    this.http.postData(ApiUrl.personalInfo, this.bookingInfoData).subscribe(response => {
      if (response.data) {
        this.router.navigate(['/payment-step-2'], {queryParams: {bookingId: this.bookingId}});
      }
    }, error => {

      this.message.showNotification('danger', error.error.msg);
      this.router.navigate(['/payment-step-2'], {queryParams: {bookingId: this.bookingId}});
    });
  }

  backScreen() {
    this.router.navigate(['/trip-detail']);
  }

}
