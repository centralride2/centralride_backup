import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {JwtInterceptor} from './common/services/interceptors/jwt.interceptor';
import {ErrorInterceptor} from './common/services/interceptors/error.interceptor';
import {LoaderInterceptor} from './common/services/interceptors/loader.interceptor';
import {GoogleplaceDirective} from './common/shared/directives/googlePlacePicker';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {NgxIntlTelInputModule} from 'ngx-intl-tel-input';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
// import { DatepickerModule } from 'angular7-material-datepicker';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatInputModule
} from '@angular/material';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomepageComponent} from './homepage/homepage.component';
import {FooterComponent} from './common/footer/footer.component';
import {HeaderComponent} from './common/header/header.component';
import {TripListingComponent} from './trip-listing/trip-listing.component';
import {TripDetailComponent} from './trip-detail/trip-detail.component';
import {PaymentStep1Component} from './payment-step1/payment-step1.component';
import {PaymentStep2Component} from './payment-step2/payment-step2.component';
import {PaymentStep3Component} from './payment-step3/payment-step3.component';
import {HeaderFilterComponent} from './common/header-filter/header-filter.component';
import {ThankYouComponent} from './thank-you/thank-you.component';
import {RegisterComponent} from './register/register.component';

import {ProfileEditComponent} from './profile-edit/profile-edit.component';
// import { BlogsComponent } from './profile/blogs/blogs.component';
import {ProfileSidebarComponent} from './profile-sidebar/profile-sidebar.component';
import {ProfileComponent} from './profile/profile.component';
import {ProfileTabsComponent} from './profile-tabs/profile-tabs.component';
import {OtpVerificationComponent} from './otp-verification/otp-verification.component';
import {VendorListingComponent} from './vendor-listing/vendor-listing.component';
import {BookingInfoComponent} from './booking-info/booking-info.component';

import {MyBookingsComponent} from './bookings/my-bookings/my-bookings.component';
import {UpcomingBookingsComponent} from './bookings/upcoming-bookings/upcoming-bookings.component';
import {PastBookingsComponent} from './bookings/past-bookings/past-bookings.component';
import {CreateBookingComponent} from './bookings/create-booking/create-booking.component';

import {MessagesComponent} from './profile/messages/messages.component';
import {AboutUsComponent} from './about-us/about-us.component';
import {ContactUsComponent} from './contact-us/contact-us.component';
import {FaqsComponent} from './faqs/faqs.component';
import {AffiliateComponent} from './affiliate/affiliate.component';
import {TncComponent} from './tnc/tnc.component';

import {VendorListComponent} from './blogs/my-list/vendor-list.component';
import {BlogListComponent} from './blogs/list/blog-list.component';
import {AddEditComponent} from './blogs/add-edit/add-edit.component';
import {PrivacyPolicyComponent} from './privacy-policy/privacy-policy.component';
import {ChangePasswordComponent} from './change-password/change-password.component';
import {LoaderComponent} from './loader/loader.component';
import {BookingComponent} from './booking/booking.component';

import {NgImageSliderModule} from 'ng-image-slider';
import {VendorDetailComponent} from './blogs/vendor-detail/vendor-detail.component';
import {BlogDetailComponent} from './blogs/blogs-detail/blog-detail.component';

import { RouterModule} from '@angular/router';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { NgxSpinnerModule } from "ngx-spinner";
import { NgxSpinnerService } from "ngx-spinner";
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';
import { MatSnackBar, MatSnackBarModule } from '@angular/material';
import { QRCodeModule } from 'angularx-qrcode';
import { NotfoundComponent } from './notfound/notfound.component';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  declarations: [
    AppComponent, HomepageComponent, FooterComponent, HeaderComponent, TripListingComponent,
    TripDetailComponent, PaymentStep1Component, PaymentStep2Component, PaymentStep3Component,
    HeaderFilterComponent, ThankYouComponent, RegisterComponent, MyBookingsComponent, ProfileEditComponent,
    // BlogsComponent,
    ProfileSidebarComponent,
    ProfileComponent,
    ProfileTabsComponent,
    OtpVerificationComponent,
    GoogleplaceDirective,
    VendorListingComponent,
    BookingInfoComponent,
    UpcomingBookingsComponent,
    PastBookingsComponent,
    MessagesComponent,
    AboutUsComponent,
    ContactUsComponent,
    FaqsComponent, BookingComponent,
    AffiliateComponent,
    VendorListComponent,
    BlogListComponent,
    TncComponent, BlogListComponent, VendorListComponent, BlogDetailComponent, VendorDetailComponent,
    AddEditComponent, LoaderComponent,
    CreateBookingComponent, VendorDetailComponent,
    PrivacyPolicyComponent,
    ChangePasswordComponent,
    NotfoundComponent

  ],
  imports: [
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    NgxIntlTelInputModule,
    // DatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    MatDatepickerModule,
    NgImageSliderModule,
    RouterModule,
    SlickCarouselModule,
    NgxSpinnerModule,
    MatSnackBarModule,
    QRCodeModule,
    MatButtonModule,
    NgxLoadingModule.forRoot({
      animationType: ngxLoadingAnimationTypes.wanderingCubes,
      backdropBackgroundColour: 'rgba(0,0,0,0.1)', 
      backdropBorderRadius: '4px',
      primaryColour: '#ffffff', 
      secondaryColour: '#ffffff', 
      tertiaryColour: '#ffffff'
  })
  ],
  providers: [NgxSpinnerService,
    // {provide: APP_BASE_HREF, useValue : '/' },
    {provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
