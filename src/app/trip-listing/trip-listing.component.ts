import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {Validators, FormGroup, FormBuilder} from '@angular/forms';

import {Subscription} from 'rxjs';

import {ApiUrl} from '../common/core/apiUrl';
import {HttpService} from '../common/services/http/http.service';
import {UserService} from '../common/services/user/user.service';
import {MessageService} from '../common/services/message/message.service';
import * as moment from 'moment';

import {DataService} from '../common/services/data/data.service';

@Component({
  selector: 'app-trip-listing',
  templateUrl: './trip-listing.component.html'
})
export class TripListingComponent implements OnInit {

  paramSubscription: Subscription;
  vid: number = 0;
  tripsSearchParams: any;
  trips: any = [];
  service_type;
  tripsCount: number = 0;
  loggedIn: boolean = false;

  constructor(private formBuild: FormBuilder, private http: HttpService, private users: UserService, private router: Router, private message: MessageService, private data: DataService, private route: ActivatedRoute) {
    this.paramSubscription = this.route.queryParams.subscribe((params: Params) => {
      this.vid = params['vid'] ? params['vid'] : 0;
    });
  }

  token: any = '';
  userDetails: any = '';

  ngOnInit() {
    this.getVendorTrips();
    this.token = this.users.getUserToken;
    this.userDetails = this.users.currentUserValue;
    if (this.userDetails) {
      this.loggedIn = true;
    }
  }

  getVendorTrips() {
    this.tripsSearchParams = JSON.parse(this.data.getSearchData);
    this.tripsSearchParams['vid'] = this.vid;
    this.http.getData(ApiUrl.getvendors, this.tripsSearchParams).subscribe(response => {
    // this.http.getData(ApiUrl.getvendors, this.tripsSearchParams).subscribe(response => {
      if (response.data) {
        this.trips = response.data.data;
        this.tripsCount = response.data.count;  
        if(this.trips[0]['service_type'] == 0)
        {
          if(this.trips[0]['doorservice'] == 0){
            this.service_type = 'direct Service';
          }else if(this.trips[0]['doorservice'] == 1){
            this.service_type = 'door to door Service';
          }else{ }
            console.log("service type 0")
        }else if(this.trips[0]['service_type'] == 1){
          this.service_type = 'both Service';
          console.log("service type 1")
        }else{}
      } else {
        this.message.showNotification('danger', response.msg);
      }

    }, error => {
      this.message.showNotification('danger', error.error.msg);
    });
  }

  // tripDetailScreen() {
  //   this.router.navigate(['/trip-detail']);
  // }

}
