import {Component, OnInit, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';
import {LoaderService} from '../common/services/loader/loader.service';
import {HttpService} from '../common/services/http/http.service';

@Component({
  selector: 'app-loader',
  templateUrl: 'loader.component.html',
  styleUrls: ['loader.component.scss']
})
export class LoaderComponent implements OnInit {

  show = false;
  private subscription: Subscription;

  constructor(private http: HttpService) {
  }

  ngOnInit() {
    this.subscription = this.http.loaderState.subscribe((state) => {
      console.log('amm heete')
      this.show = state.show;
    });
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
