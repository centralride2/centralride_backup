import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {Subscription} from 'rxjs';
import {ApiUrl} from '../common/core/apiUrl';
import {MessageService} from '../common/services/message/message.service';
import {HttpService} from '../common/services/http/http.service';

@Component({
  selector: 'app-booking-info',
  templateUrl: './booking-info.component.html'
})
export class BookingInfoComponent implements OnInit {

  paramSubscription: Subscription;
  bookingId: number = 0;
  booking: any;

  constructor(private http: HttpService, private router: Router, private route: ActivatedRoute, private message: MessageService) {
    this.paramSubscription = this.route.queryParams.subscribe((params: Params) => {
      this.bookingId = params['bookingId'] ? params['bookingId'] : 0;
    });
  }

  ngOnInit() {
    this.getBookingDetails();
  }

  getBookingDetails() {
    if (!this.bookingId) {
      this.router.navigate(['/vendor-listing']);
    }

    this.http.getData(ApiUrl.bookingDetail + '/' + this.bookingId + '/detail', {}).subscribe(response => {
      if (response.data) {
        console.log('respponse data', response.data);
        this.booking = response.data.booking;
      } else {
        this.message.showNotification('danger', response.msg);
      }

    }, error => {
      this.message.showNotification('danger', error.error.msg);
    });
  }

}
