import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ApiUrl} from '../common/core/apiUrl';
import {HttpService} from '../common/services/http/http.service';

@Component({
  selector: 'app-tnc',
  templateUrl: './tnc.component.html'
})
export class TncComponent implements OnInit {

  TnCHtml: any;

  constructor(private http: HttpService, private router: Router) {
  }

  ngOnInit() {
    this.getTnCPage();
  }

  getTnCPage() {
    let obj = {
      page: 'terms-conditions'
    };
    this.http.postData(ApiUrl.pages, obj).subscribe(response => {
      if (response.data) {
        this.TnCHtml = response.data.content;
      } else {
        // this.message.showNotification('danger', response.msg);
      }

    }, error => {
      // this.message.showNotification('danger', error.error.msg);
    });
  }

}
