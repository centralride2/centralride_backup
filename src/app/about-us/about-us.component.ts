import {Component, OnInit} from '@angular/core';
import {ApiUrl} from '../common/core/apiUrl';
import {HttpService} from '../common/services/http/http.service';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html'
})
export class AboutUsComponent implements OnInit {

  aboutUsHtml: any;

  constructor(private http: HttpService) {
  }

  ngOnInit() {
    this.getAboutUsPage();
  }

  getAboutUsPage() {
    let obj = {
      page: 'about-us'
    };
    this.http.postData(ApiUrl.pages, obj).subscribe(response => {
      if (response.data) {
        this.aboutUsHtml = response.data.content;
      } else {
        // this.message.showNotification('danger', response.msg);
      }

    }, error => {
      // this.message.showNotification('danger', error.error.msg);
    });
  }
}
