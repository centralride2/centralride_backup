import {Component, OnInit} from '@angular/core';

import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';

import {ApiUrl} from '../common/core/apiUrl';
import {HttpService} from '../common/services/http/http.service';
import {UserService} from '../common/services/user/user.service';
import {MessageService} from '../common/services/message/message.service';
import {Subscription} from 'rxjs';

declare const Stripe: any;
declare var $: any;

@Component({
  selector: 'app-payment-step3',
  templateUrl: './payment-step3.component.html'
})
export class PaymentStep3Component implements OnInit {

  paramSubscription: Subscription;
  card: any;
  stripe: any;
  bookingId: number = 0;
  cardDetails: any = {};
  paymentType: string = 'card';
  cardList: any;

  constructor(private formBuild: FormBuilder, private http: HttpService, private users: UserService, private router: Router, private message: MessageService, private route: ActivatedRoute) {
    this.paramSubscription = this.route.queryParams.subscribe((params: Params) => {
      this.bookingId = params['bookingId'] ? params['bookingId'] : 0;
    });
  }

  ngOnInit() {
    this.loadCardForm();
    this.getCardList();
  }

  loadCardForm() {
    this.stripe = Stripe('pk_test_MbL3FTLo9WKOcqqqg625xLZm');
    const elements = this.stripe.elements();
    var style = {
      base: {
        color: '#515151',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
          color: '#ccc'
        }
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
      }
    };
    this.card = elements.create('card', {style: style});
    this.card.mount('#card-element');

  }

  backScreen() {
    this.router.navigate(['/payment-step-2'], {queryParams: {bookingId: this.bookingId}});
  }

  confirmScreen() {
    this.router.navigate(['/thank-you']);
  }

  async createStripeToken() {
    if (this.paymentType === 'card') {

      console.log(this.selectedCard, 'selectedCard');
      if (this.selectedCard) {
        let obj = {
          payment_source: 'savedcard',
          booking_id: this.bookingId,
          card_id: this.selectedCard.card_id
        };
        this.finalPayment(obj);
      } else {
        this.stripe.createToken(this.card).then((result) => {
          if (result.error) {
            var errorElement = document.getElementById('card-errors');
            errorElement.textContent = result.error.message;
          } else {
            console.log('result.tokenresult.token', result.token);
            this.initiatePayment(result.token.id);
          }
        });
      }
    } else {
      this.initiatePayment('');
    }

  }

  finalPayment(obj) {

    this.http.postData(ApiUrl.stripePayment, obj).subscribe(response => {
      if (response.data) {
        this.message.showNotification('success', 'Payment Success');
        this.router.navigate(['/thank-you']);
      }
    }, error => {
      this.message.showNotification('danger', error.error.msg);
    });
  }

  initiatePayment(stripeToken) {
    let obj = {
      payment_source: this.paymentType,
      booking_id: this.bookingId
    };

    if (this.paymentType === 'card') {
      obj['stripe_payment_method_id'] = stripeToken;
    }
    this.http.postData(ApiUrl.stripePayment, obj).subscribe(res => {
      if (res.data) {
        this.router.navigate(['/thank-you']);
        // this.message.showNotification('danger', res.error.msg);
      }
    }, error => {
      this.message.showNotification('danger', error.error.msg);
    });
  }

  getCardList() {
    this.http.getData(ApiUrl.allCards, {}).subscribe(res => {
      if (res.data) {
        this.cardList = res.data.cards;
        // this.router.navigate(['/thank-you']);
        // this.message.showNotification('danger', res.error.msg);
      }
    }, error => {
      this.message.showNotification('danger', error.error.msg);
    });
  }

  paymentMode(value) {
    this.paymentType = value;
    if (value === 'card') {
      $('.pay-form').show();
    } else {
      $('.pay-form').hide();
    }
  }

  selectedCard: any = '';

  selected(data) {
    this.selectedCard = data;
    console.log('fataftaa', data);
  }

}
