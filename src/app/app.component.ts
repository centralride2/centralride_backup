import {Component} from '@angular/core';
import {HttpService} from './common/services/http/http.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'shuttle-website';

  public isLoading : boolean = false;

  constructor(private httpService : HttpService ) {
		this.httpService.showLoader.subscribe((isLoading: any) => {
			setTimeout(() => this.isLoading = isLoading, 0)
    });
}
  
}
