import {Component, OnInit} from '@angular/core';

import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';

import {ApiUrl} from '../common/core/apiUrl';
import {HttpService} from '../common/services/http/http.service';
import {UserService} from '../common/services/user/user.service';
import {MessageService} from '../common/services/message/message.service';
import {SearchCountryField, TooltipLabel, CountryISO} from 'ngx-intl-tel-input';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html'
})
export class ProfileEditComponent implements OnInit {

  personalInfo: FormGroup;
  profileData: Object = {};
  separateDialCode = true;
  SearchCountryField = SearchCountryField;
  TooltipLabel = TooltipLabel;
  CountryISO: CountryISO | string = CountryISO.UnitedStates;
  preferredCountries: CountryISO[] = [CountryISO.UnitedStates, CountryISO.UnitedKingdom];
  userDetails: object = {};
  show: boolean = false;

  
  constructor(private formBuild: FormBuilder, private http: HttpService, private users: UserService, private router: Router, private message: MessageService, private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.bookingInfoForm();
    this.userDetails = this.users.currentUserValue;
    if (this.userDetails) {
      let obj = {
        name: this.userDetails['name'],
        email: this.userDetails['email'],
        number: this.userDetails['mobile'],
        // address: ''
      };
      this.CountryISO = ('US').toLowerCase(); 
      this.personalInfo.patchValue(obj);
    }
  }

  bookingInfoForm() {
    this.personalInfo = this.formBuild.group({
      'name': ['', Validators.required],
      'email': ['', [Validators.required, Validators.email]],
      'number': ['', Validators.required],
      'address': ['', []]
    });
  }

  submit(valid, value) {
    console.log('value', value);
    this.show = true;
    if (valid) {
      this.updateProfile(value);
    } else {
      console.log('incomplete');
    }
  }

  updateProfile(value) {
    this.profileData = value;
    console.log("profile data" , this.profileData )
    this.profileData['calling_code'] = this.profileData['number']['dialCode'];
    this.profileData['mobile'] = this.profileData['number']['number'];

    delete this.profileData['number'];

    this.http.postData(ApiUrl.editProfile, this.profileData).subscribe(response => {
      console.log("edit profile" , response)
      if (response.data) {
        this.users.setUserLocalData(response.data);
        this.http.openSnackBar(response.msg, true);
        // this.message.showNotification('success', response.msg);
      } else {
        this.http.openSnackBar(response.msg, true);
        // this.message.showNotification('danger', response.msg);
      }
    }, error => {
      // this.http.openSnackBar(response.msg, true);
      // this.message.showNotification('danger', error.error.msg);
    });
  }

}
