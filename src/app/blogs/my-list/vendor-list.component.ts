import {Component, OnInit} from '@angular/core';
import {ApiUrl} from '../../common/core/apiUrl';
import {HttpService} from '../../common/services/http/http.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-vendor-list',
  templateUrl: './vendor-list.component.html'
})
export class VendorListComponent implements OnInit {

  constructor(public http: HttpService, public router: Router) {
  }

  ngOnInit() {
    this.vendorList();
  }

  allVendors: any = [];

  vendorList() {
    this.http.getData(ApiUrl.allVendors, {}).subscribe(res => {
      this.allVendors = res.data;
    }, () => {
    });
  }

  openVendor(data) {
    localStorage.setItem('vendorData', JSON.stringify(data));
    this.router.navigate(['/vendor-detail']);
  }
}
