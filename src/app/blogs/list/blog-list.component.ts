import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { ApiUrl } from '../../common/core/apiUrl';
import { HttpService } from '../../common/services/http/http.service';
import { Subscriber } from 'rxjs';

@Component({
  selector: 'app-list',
  templateUrl: './blog-list.component.html'
})
export class BlogListComponent implements OnInit, OnDestroy {
  blogData: any = [];
  category: any = [];
  subscription = new Subscriber();

  constructor(public router: Router, public http: HttpService) {
    this.getCategories();
  }

  getCategories() {

    let request = {
      limit: 1,
    }

    this.subscription.add(this.http.getData(ApiUrl.blogCategory, request).subscribe(res => {
      if (res.data.length) {
        this.category = res.data;
        this.category.map(x => { x.isActive = false });
        this.getBlogData(this.category[0]);
      }
    }))
  }

  ngOnInit() {
  }

  getBlogData(cat?) {
    let request = {
      category: cat.id,
      limit: 1,
    }

    this.category.forEach(element => {
      element.isActive = false;
      if (element.id == cat.id) {
        element.isActive = true;
      }
    });

    this.http.getData(ApiUrl.blogs, request).subscribe(res => {
      console.log("ggggg" , res)
      this.blogData = res.data;
    });
  }

  openBlog(data) {
    var id = '';
    this.category.forEach(element => {
      if (element.isActive) {
        id = element.id
      }
    });
    this.router.navigate(['/blog-detail/' + data.slug + '/' + id]);
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
