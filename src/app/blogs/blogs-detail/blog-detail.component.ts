import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpService } from '../../common/services/http/http.service';
import { UserService } from '../../common/services/user/user.service';
import { DataService } from '../../common/services/data/data.service';
import { MessageService } from '../../common/services/message/message.service';
import { Subscriber } from 'rxjs';
import { ApiUrl } from 'src/app/common/core/apiUrl';
declare var $: any;
@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.css']
})
export class BlogDetailComponent implements OnInit, AfterViewInit {

  blog;
  private subscription = new Subscriber();
  blogName;
  catId;
  constructor(private activatedRoute: ActivatedRoute, private http: HttpService) {
    this.subscription.add(this.activatedRoute.params.subscribe(params => {
      if (params.name) {
        this.blogName = params.name;
        this.catId = params.id ? params.id : ''
        this.getBlogDetails(params.name, this.catId);
      }else{
        this.blog = JSON.parse(localStorage.getItem('blogData'));
        console.log("blog" , this.blog)
      }
    }))
  }

  getBlogDetails(name?, id?) {
    const request = {
      slug: name,
      category: id
    }
    this.http.getData(ApiUrl.blogDetails, request).subscribe(res => {
      this.blog = res.data;
    });
  }

  ngOnInit() {
    // this.blog = JSON.parse(localStorage.getItem('blogData'));
  }

  ngAfterViewInit() {
    const share = document.querySelector('.share');

    setTimeout(() => {
      share.classList.add("hover");
    }, 1000);

    setTimeout(() => {
      share.classList.remove("hover");
    }, 3000);
  }

}
