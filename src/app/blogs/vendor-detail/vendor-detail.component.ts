import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-vendor-detail',
  templateUrl: './vendor-detail.component.html',
  styleUrls: ['./vendor-detail.component.css']
})
export class VendorDetailComponent implements OnInit {

  blog;

  constructor() {
  }

  ngOnInit() {
    this.blog = JSON.parse(localStorage.getItem('vendorData'));
    console.log(this.blog,' this.blog');
  }
}
