import {Component, OnInit} from '@angular/core';
import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {ApiUrl} from '../common/core/apiUrl';
import {HttpService} from '../common/services/http/http.service';
import {MessageService} from '../common/services/message/message.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html'
})

export class ContactUsComponent implements OnInit {

  contactForm: FormGroup;
  contactUsHtml: any;
  contactFormData: any = {};
  show: boolean = false;
  contactInfo: any = {};

  constructor(private formBuild: FormBuilder, private http: HttpService, private router: Router, private message: MessageService) {
  }

  ngOnInit() {
    this.contactFormGeneration();
    this.infoDetails();
  }

  contactFormGeneration() {
    this.contactForm = this.formBuild.group({
      'name': ['', Validators.required],
      'email': ['', [Validators.required, Validators.email]],
      'phone': ['', Validators.required],
      'message': ['', Validators.required]
    });
  }

  submit(valid, value): void {
    this.show = true;
    if (valid) {
      this.contactUs(value);
    } else {
      console.log('invalid');
    }

  }

  contactUs(value) {
    this.contactFormData = value;
    this.http.postData(ApiUrl.contactUs, value).subscribe(response => {
      console.log('responseresponse', response);
      if (response.data) {
        this.message.showNotification('success', 'Enquiry Submitted!');
      } else {
        this.message.showNotification('danger', response.msg);
      }

    }, error => {
      this.message.showNotification('danger', error.error.msg);
    });
  }

  infoDetails() {

    this.http.getData(ApiUrl.contactUs, {}).subscribe(response => {
      if (response.data) {
        this.contactInfo = response.data.contact_details;
      } else {
      }
    }, error => {
    });
  }

}
