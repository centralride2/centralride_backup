import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {Validators, FormGroup, FormBuilder} from '@angular/forms';

import {Subscription} from 'rxjs';

import {ApiUrl} from '../common/core/apiUrl';
import {HttpService} from '../common/services/http/http.service';
import {UserService} from '../common/services/user/user.service';
import {MessageService} from '../common/services/message/message.service';
import * as moment from 'moment';

import {DataService} from '../common/services/data/data.service';

@Component({
  selector: 'app-trip-detail',
  templateUrl: './trip-detail.component.html'
})
export class TripDetailComponent implements OnInit {

  paramSubscription: Subscription;
  vid: number = 0;
  trip_id: number = 0;
  tripsSearchParams: any;
  tripDetail: any;

  tripFinalPrice: any;
  promoList: any = [];
  tripsCount: number = 0;
  totalSeatBooking: number = 1;
  totalBags: number = 0;
  bagCharges: number = 0;
  perSeatPrice: number = 0;
  totalAmount: number = 0;
  totalCost: number = 0;
  cancellationCharge: number = 0;

  tripDetailData: any = {};

  constructor(private formBuild: FormBuilder, private http: HttpService, private users: UserService, private router: Router, private message: MessageService, private data: DataService, private route: ActivatedRoute) {
    this.paramSubscription = this.route.queryParams.subscribe((params: Params) => {
      this.vid = params['vid'] ? params['vid'] : 0;
      this.trip_id = params['trip_id'] ? params['trip_id'] : 0;
    });
  }

  ngOnInit() {
    this.getTripDetail();
    this.getFinalPrice();
    this.getDiscountPromos();
    this.previousDues();
  }

  getTripDetail() {
    if (!this.trip_id) {
      this.router.navigate(['/vendor-listing']);
    }

    this.tripDetailData = JSON.parse(this.data.getSearchData);
    this.http.getData(ApiUrl.getTripDetail + '/' + this.trip_id, {}).subscribe(response => {
      if (response.data) {
        console.log('respponse data', response.data);
        this.tripDetail = response.data.trip;
        this.perSeatPrice = this.tripDetail['price'];
      } else {
        this.message.showNotification('danger', response.msg);
      }

    }, error => {
      this.message.showNotification('danger', error.error.msg);
    });
  }

  getFinalPrice() {
    if (!this.trip_id) {
      this.router.navigate(['/vendor-listing']);
    }

    let finalPriceParams = JSON.parse(this.data.getSearchData);
    finalPriceParams['trip_id'] = this.trip_id;
    delete finalPriceParams['date'];
    delete finalPriceParams['start'];
    delete finalPriceParams['end'];
    this.http.getData(ApiUrl.getFinalPrice, finalPriceParams).subscribe(response => {
      if (response.statuscode == 200) {
        this.tripFinalPrice = response.data;

        if (this.tripFinalPrice != null) {
          if (this.tripFinalPrice > 0) {
            this.perSeatPrice = this.tripFinalPrice + this.tripDetail['price'];
          } else {
            this.perSeatPrice = this.tripDetail['price'];
          }
        } else {
          this.perSeatPrice = this.tripDetail['price'];
        }
        this.seatBooking('');
      } else {
        this.message.showNotification('danger', response.msg);
      }

    }, error => {
      this.message.showNotification('danger', error.error.msg);
    });
  }

  getDiscountPromos() {
    this.http.getData(ApiUrl.promoCodelist, {}).subscribe(response => {
      if (response.data) {
        this.promoList = response.data;
      } else {
        this.message.showNotification('danger', response.msg);
      }

    }, error => {
      this.message.showNotification('danger', error.error.msg);
    });
  }

  previousDues() {
    this.http.getData(ApiUrl.paymentDues, {}).subscribe(response => {
      if (response.data) {
        this.cancellationCharge = response.data.previous_due.amount;
        this.totalCostCalculation();
      } else {
        this.message.showNotification('danger', response.msg);
      }

    }, error => {
      this.message.showNotification('danger', error.error.msg);
    });
  }

  seatBooking(type) {
    if (type == 'ADD') {
      if (this.totalSeatBooking < this.tripDetail['rseats']) {
        this.totalSeatBooking = this.totalSeatBooking + 1;
      }

    }
    if (type == 'SUB') {
      if (this.totalSeatBooking > 1) {
        this.totalSeatBooking = this.totalSeatBooking - 1;
      }
    }

    this.totalAmount = this.totalSeatBooking * this.perSeatPrice;
    this.totalCostCalculation();
  }

  numberOfBags(type) {
    if (type == 'ADD') {
      this.totalBags = this.totalBags + 1;
    }
    if (type == 'SUB') {
      if (this.totalBags > 0) {
        this.totalBags = this.totalBags - 1;
      }
    }
    this.bagCharges = this.totalBags * this.tripDetail['bagsprice'];
    this.totalCostCalculation();
  }

  paymentStage1Screen() {
    this.router.navigate(['/payment-step-1']);
  }

  totalCostCalculation() {
    this.totalCost = this.bagCharges + this.totalAmount;

    if (this.cancellationCharge) {
      this.totalCost = this.totalCost + this.cancellationCharge;
    }
  }

  createBooking() {
    let bookingData = JSON.parse(this.data.getSearchData);
    bookingData['vendor_id'] = this.vid;
    bookingData['trip_id'] = this.trip_id;
    bookingData['seats'] = this.totalSeatBooking;
    bookingData['bags'] = this.totalBags;
    bookingData['totalprice'] = this.totalCost;
    this.http.postData(ApiUrl.createbooking, bookingData).subscribe(response => {
      if (response.data) {
        this.router.navigate(['/payment-step-1'], {queryParams: {bookingId: response.data.id}});
      } else {
        this.message.showNotification('danger', response.msg);
      }

    }, error => {
      this.message.showNotification('danger', error.error.msg);
    });
  }

}
