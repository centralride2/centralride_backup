import {Component, OnInit} from '@angular/core';
import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';

import {ApiUrl} from '../common/core/apiUrl';
import {HttpService} from '../common/services/http/http.service';
import {UserService} from '../common/services/user/user.service';
import {MessageService} from '../common/services/message/message.service';

declare var $: any;

@Component({
  selector: 'app-otp-verification',
  templateUrl: './otp-verification.component.html'
})
export class OtpVerificationComponent implements OnInit {

  otpForm: FormGroup;
  registrationFinalData: Object = {};

  constructor(private formBuild: FormBuilder, private http: HttpService, private users: UserService, private router: Router, private message: MessageService) {
  }

  ngOnInit() {
    this.otpVerify();
  }

  otpVerify() {
    this.otpForm = this.formBuild.group({
      'otp': ['', Validators.required]
    });
  }

  otpSubmission(valid, value): void {
    if (valid) {
      this.registrationCompletion(value);
    }
  }

  registrationCompletion(value) {
    this.registrationFinalData = this.users.getRegisterationData;
    this.registrationFinalData['code'] = parseInt(value.otp);
    this.registrationFinalData['mobile'] = this.registrationFinalData['mobile'].replace(' ', '');
    console.log('this.registrationFinalData', this.registrationFinalData);
    $('#verifyotp').modal('hide');
    this.http.postData(ApiUrl.register, this.registrationFinalData).subscribe(response => {

      console.log('responseresponse', response);
      if (response.data) {
        this.http.openSnackBar( 'Successfully Registered!' , true);
        // this.message.showNotification('success', 'Successfully Registered!');

        this.users.setUserLocalData(response.data);
        // this.users.removeRegisterData();
        document.location.reload()
        // if(document.location.pathname === "/homepage"){
        //   this.router.navigate(['/profile']);
        // }

      } else {
        this.message.showNotification('danger', response.msg);
        // this.router.navigate(['/login']);
      }

    }, error => {
      $('#verifyotp').modal('hide');
      this.http.openSnackBar(error.error.msg, true);
      window.location.reload();
      // this.message.showNotification('danger', error.error.msg);
    });
  }

}
