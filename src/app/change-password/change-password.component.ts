import {Component, OnInit} from '@angular/core';
import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {ApiUrl} from '../common/core/apiUrl';
import {HttpService} from '../common/services/http/http.service';
import {UserService} from '../common/services/user/user.service';
import {MessageService} from '../common/services/message/message.service';
import {Subscription} from 'rxjs';
declare const Stripe: any;
declare var $: any;

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
})
export class ChangePasswordComponent implements OnInit {

  changePasswordForm: FormGroup;
  show: boolean = false;

  constructor(private formBuild: FormBuilder, private http: HttpService, private users: UserService, private router: Router, private message: MessageService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.changePassword();
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPass.value;
    return pass === confirmPass ? null : {notSame: true};
  }

  changePassword() {
    this.changePasswordForm = this.formBuild.group({
      'password': ['', [Validators.required, Validators.minLength(8)]],
      'confirmPass': ['', Validators.required]
    }, {validator: this.checkPasswords});
  }

  submit(valid, value): void {
    console.log('here', value);
    console.log('this.confirmPass', this.changePasswordForm.controls['confirmPass']);
    this.show = true;
    if (valid) {
      this.changingPasswordApi(value);
    } else {
      console.log('invalud');
    }
  }

  changingPasswordApi(data) {
    let obj = data;
    delete obj['confirmPass'];
    this.http.postData(ApiUrl.changePassword, obj).subscribe(response => {
      if (response.data) {
        this.message.showNotification('success', 'Password Successfully Changed!');
      }
    }, error => {
      this.message.showNotification('danger', error.error.msg);
    });
  }

}
