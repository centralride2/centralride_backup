import {Component, OnInit} from '@angular/core';
import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {ApiUrl} from '../../common/core/apiUrl';
import {HttpService} from '../../common/services/http/http.service';
import {UserService} from '../../common/services/user/user.service';
import {MessageService} from '../../common/services/message/message.service';
import {DataService} from '../../common/services/data/data.service';
// import { environment } from 'src/environments/environment';
import {environment} from '../../../environments/environment';
declare var $: any;

@Component({
  selector: 'app-upcoming-bookings',
  templateUrl: './upcoming-bookings.component.html'
})
export class UpcomingBookingsComponent implements OnInit {

  cancellationForm: FormGroup;
  bookingParams: object = {};
  bookings: any = [];
  reasons: any = [];
  bookingsCount: number = 0;
  bookingId: number = 0;
  imageUrl;
  status:any;
  
  constructor(private formBuild: FormBuilder, private http: HttpService, private users: UserService
    , private router: Router, private message: MessageService, private data: DataService
    ) {
      this.imageUrl = environment.apiBaseUrl;
  }

  ngOnInit() {
    this.getBookings();
    this.cancellationReasons();
    this.cancellationReasonFormGenerate();
  }

  cancellationReasonFormGenerate() {
    this.cancellationForm = this.formBuild.group({
      'reason_id': ['', Validators.required]
    });
  }

  getBookings() {
    this.bookingParams = {
      type: '1',
      pageno: '1',
      pageoffset: '10'
    };
    this.http.getData(ApiUrl.getBookings, this.bookingParams).subscribe(response => {
      console.log("upcomig ", response);
      if (response.data) {
        this.bookings = response.data.data;
        console.log(this.bookings , "date")
        this.bookingsCount = response.data.count;
        for(let i of this.bookings){
            if(i.driver_status == 0){
                this.status = 'pending';
            }else if(i.driver_status == 1){
                this.status = 'Driver Accepted Trip';
            }else if(i.driver_status == 2){
                this.status = 'Driver Started The Trip';
            }else if(i.driver_status == 3){
                this.status = 'Driver On The Way';
            }else if(i.driver_status == 4){
                this.status = 'Driver Pickup';
            }else if(i.driver_status == 5){
                this.status = 'Driver Reached'
            }else{}
        }
      } else {
        this.message.showNotification('danger', response.msg);
      }
      
    }, error => {
      this.message.showNotification('danger', error.error.msg);
    });
    
  }

  cancellationReasons() {
    this.http.getData(ApiUrl.cancellationReasons, {}).subscribe(response => {
      if (response.data) {
        this.reasons = response.data.reasons;
      } else {
        this.http.openSnackBar(response.msg, true);
        $('#reason').modal('hide'); 
      }

    }, error => {
      this.http.openSnackBar(error.error.msg, true);
      $('#reason').modal('hide');
    });
  }

  openModal(bookingId) {
    this.bookingId = bookingId;
    console.log('{{booking?.end}}', this.bookingId);
    $('#reason').modal('show');
  }

  submitReason(valid, value) {
    if (valid) {
      this.submitCancellationReason(value);
    }

  }

  submitCancellationReason(value) {
    let obj = {
      booking_id: this.bookingId.toString(),
      reason_id: value.reason_id
    };

    this.http.postData(ApiUrl.cancelbooking, obj).subscribe(response => {
      console.log('responseresponse', response);
      if (response.data) {
        this.http.openSnackBar('Booking Cancelled Successfully!', true);
        $('#reason').modal('hide');
      } else {
        this.http.openSnackBar(response.msg, true);
        $('#reason').modal('hide');
      }
    }, error => {
      this.http.openSnackBar(error.error.msg, true);
      $('#reason').modal('hide');
    });

  }

  gotoBooking(data) {
    localStorage.setItem('bookingData', JSON.stringify(data));
    this.router.navigate(['/booking']);
  }

}
