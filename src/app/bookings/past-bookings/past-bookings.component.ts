import {Component, OnInit} from '@angular/core';
import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {ApiUrl} from '../../common/core/apiUrl';
import {HttpService} from '../../common/services/http/http.service';
import {UserService} from '../../common/services/user/user.service';
import {MessageService} from '../../common/services/message/message.service';
import {DataService} from '../../common/services/data/data.service';
import {environment} from '../../../environments/environment';

declare var $: any;

@Component({
  selector: 'app-past-bookings',
  templateUrl: './past-bookings.component.html'
})
export class PastBookingsComponent implements OnInit {



  cancellationForm: FormGroup;
  bookingParams: object = {};
  bookings: any = [];
  reasons: any = [];
  bookingsCount: number = 0;
  bookingId: number = 0;
  imageUrl;
  status;

  constructor(private formBuild: FormBuilder, private http: HttpService, private users: UserService, 
    private router: Router, private message: MessageService, private data: DataService) {
      this.imageUrl = environment.apiBaseUrl;
  }

  ngOnInit() {
    this.getBookings();
    this.cancellationReasons();
    this.cancellationReasonFormGenerate();
  }

  cancellationReasonFormGenerate() {
    this.cancellationForm = this.formBuild.group({
      'reason_id': ['', Validators.required]
    });
  }

  getBookings() {
    this.bookingParams = {
      type: '2',
      pageno: '1',
      pageoffset: '10'
    };
    this.http.getData(ApiUrl.getBookings, this.bookingParams).subscribe(response => {
      console.log("post" , response)
      if (response.data) {
        this.bookings = response.data.data;
        this.bookingsCount = response.data.count;
        for(let i of this.bookings){
          if(i.driver_status == 6){
                this.status = 'Driver Trip Ended'
          }else if(i.driver_status == 7){
            this.status = 'Driver Cancelled'
          }else{}
        } 
      } else {
        this.message.showNotification('danger', response.msg);
      }

    }, error => {
      this.message.showNotification('danger', error.error.msg);
    });
  }

  cancellationReasons() {
    this.http.getData(ApiUrl.cancellationReasons, {}).subscribe(response => {
      if (response.data) {
        this.reasons = response.data.reasons;
      } else {
        this.message.showNotification('danger', response.msg);
      }

    }, error => {
      this.message.showNotification('danger', error.error.msg);
    });
  }

  openModal(bookingId) {
    this.bookingId = bookingId;
    console.log('{{booking?.end}}', this.bookingId);
    $('#reason').modal('show');
  }

  submitReason(valid, value) {
    if (valid) {
      this.submitCancellationReason(value);
    }

  }

  submitCancellationReason(value) {
    let obj = {
      booking_id: this.bookingId.toString(),
      reason_id: value.reason_id
    };

    this.http.postData(ApiUrl.cancelbooking, obj).subscribe(response => {
      console.log('responseresponse', response);
      if (response.data) {
        this.message.showNotification('success', 'Booking Cancelled Successfully!');
      } else {
        this.message.showNotification('danger', response.msg);
      }
    }, error => {
      this.message.showNotification('danger', error.error.msg);
    });

  }

  gotoBooking(data) {
    localStorage.setItem('bookingData', JSON.stringify(data));
    this.router.navigate(['/booking']);
  }

}
