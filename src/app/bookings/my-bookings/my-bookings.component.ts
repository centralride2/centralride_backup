import {Component, OnInit} from '@angular/core';
import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {ApiUrl} from '../../common/core/apiUrl';
import {HttpService} from '../../common/services/http/http.service';
import {UserService} from '../../common/services/user/user.service';
import {MessageService} from '../../common/services/message/message.service';
import {DataService} from '../../common/services/data/data.service';

declare var $: any;

@Component({
  selector: 'app-my-bookings',
  templateUrl: './my-bookings.component.html'
})
export class MyBookingsComponent implements OnInit {

  cancellationForm: FormGroup;
  bookingParams: object = {};
  bookings: any = [];
  reasons: any = [];
  bookingsCount: number = 0;
  bookingId: number = 0;

  constructor(private formBuild: FormBuilder, private http: HttpService, private users: UserService, private router: Router, private message: MessageService, private data: DataService) {
  }

  ngOnInit() {
    this.getBookings();
    this.cancellationReasons();
    this.cancellationReasonFormGenerate();
  }

  cancellationReasonFormGenerate() {
    this.cancellationForm = this.formBuild.group({
      'reason_id': ['', Validators.required]
    });
  }

  getBookings() {
    this.bookingParams = {
      type: '1',
      pageno: '1',
      pageoffset: '10'
    };
    this.http.getData(ApiUrl.getBookings, this.bookingParams).subscribe(response => {
      if (response.data) {
        this.bookings = response.data.data;
        this.bookingsCount = response.data.count;
      } else {
        this.message.showNotification('danger', response.msg);
      }

    }, error => {
      this.message.showNotification('danger', error.error.msg);
    });
  }

  cancellationReasons() {
    this.http.getData(ApiUrl.cancellationReasons, {}).subscribe(response => {
      if (response.data) {
        this.reasons = response.data.reasons;
      } else {
        this.message.showNotification('danger', response.msg);
      }

    }, error => {
      this.message.showNotification('danger', error.error.msg);
    });
  }

  openModal(bookingId) {
    this.bookingId = bookingId;
    console.log('{{booking?.end}}', this.bookingId);
    $('#reason').modal('show');
  }

  submitReason(valid, value) {
    if (valid) {
      this.submitCancellationReason(value);
    }

  }

  submitCancellationReason(value) {
    let obj = {
      booking_id: this.bookingId.toString(),
      reason_id: value.reason_id
    };

    this.http.postData(ApiUrl.cancelbooking, obj).subscribe(response => {
      console.log('responseresponse', response);
      if (response.data) {
        this.message.showNotification('success', 'Booking Cancelled Successfully!');
      } else {
        this.message.showNotification('danger', response.msg);
      }
    }, error => {
      this.message.showNotification('danger', error.error.msg);
    });

  }

}
