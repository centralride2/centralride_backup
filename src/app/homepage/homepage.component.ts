import {Component, OnInit} from '@angular/core';
import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {ApiUrl} from '../common/core/apiUrl';
import {HttpService} from '../common/services/http/http.service';
import {UserService} from '../common/services/user/user.service';
import {MessageService} from '../common/services/message/message.service';
import * as moment from 'moment';
import {DataService} from '../common/services/data/data.service';
import { NgxSpinnerService } from "ngx-spinner";
// import * as $ from "jquery";
declare var $: any;

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html'
})
export class HomepageComponent implements OnInit {

  vendorSearchForm: FormGroup;
  vendorSearchParams: Object = {};
  trendingTrips: any = [];
  today = new Date();
  
  constructor(private formBuild: FormBuilder, private http: HttpService, private users: UserService,
              private router: Router, private message: MessageService, private data: DataService,
              private spinner: NgxSpinnerService) {
  }

  ngOnInit() {
    // this.http.show();
    this.mainSearch();
    //this.getTrendingTrips();
    this.vendorList();
    //this.getHomeData();
    this.getBlogData();
    this.getsitefeedback();
    this.getWebHomeData();
    // this.spinner.show();
  }

  slideConfig = {
  dots: true,
  infinite:true,
  speed: 1000,
  slidesToShow: 4,
  slidesToScroll: 1,
  // autoplay: true,
  autoplaySpeed: 1000,
  loop: true,
  arrows: true};


  setSlider(className) {
    $(document).ready(function ($) {
      $(className).slick({
        dots: false,
        infinite:false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        // autoplay: true,
        autoplaySpeed: 2000,
        loop: true,
        arrows: true,
        responsive: [{
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true          }
        },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 1
            }
          }]
      });
    });
  }

  searchApiParams() {
    this.vendorSearchParams = {
      start: '',
      startlat: '',
      startlong: '',
      end: '',
      endlat: '',
      endlong: '',
      date: '',
      doorservice: '',
      daytime: 3,
      search: '',
      sortbyprice: 1,
      pageno: 1,
      pageoffset: 10
    };
  }

  mainSearch() {
    this.vendorSearchForm = this.formBuild.group({
      'pickupLocation': ['', Validators.required],
      'dropoffLocation': ['', Validators.required],
      'tripDate': ['', Validators.required]
    });
  }

  homeSearch(valid, value): void {
    if (valid) {
      this.getVendors(value);
    } else {
      if (!value.pickupLocation) {
        this.http.openSnackBar('Please enter pickup location', true);
        // this.message.showNotification('danger', 'Please enter pickup location');
      } else if (!value.dropoffLocation) {
        this.http.openSnackBar('Please enter drop off location', true);
        // this.message.showNotification('danger', 'Please enter drop off location');
      } else if (!value.tripDate) {
        this.http.openSnackBar('Please enter trip date', true);
        // this.message.showNotification('danger', 'Please enter trip date');
      }
    }
  }

  addressSearch(event: any, field) {
    if (field == 'pickup') {
      this.vendorSearchParams['start'] = event.formatted_address;
      this.vendorSearchParams['startlat'] = event.lat;
      this.vendorSearchParams['startlong'] = event.lng;
    }

    if (field == 'dropoff') {
      this.vendorSearchParams['end'] = event.formatted_address;
      this.vendorSearchParams['endlat'] = event.lat;
      this.vendorSearchParams['endlong'] = event.lng;
    }
  }

  getVendors(value) {
    this.vendorSearchParams['date'] = moment(value.tripDate).format('YYYY-MM-DD');
    this.data.setSearchData(this.vendorSearchParams);

    this.router.navigate(['/vendor-listing']);
    // trip-listing?vid=410
    //     [routerLink]="['/trip-listing']" [queryParams]="{vid: booking?.vendor_id}"
    // this.router.navigate(['/trip-listing', {vid: 410}]);
  }

  getTrendingTrips() {
    let obj = {};
    this.http.getData(ApiUrl.getTrending, obj).subscribe(response => {
      if (response.data) {
        this.trendingTrips = response.data.trips;
      } else {
      }
    }, error => {
    });
  }

  allVendors;

  vendorList() {
    this.http.getData(ApiUrl.allVendors, {}).subscribe(res => {
      if (res.data) {
        this.allVendors = res.data;
        this.setSlider('.trending-slider');
      } else {
        this.message.showNotification('danger', res.msg);
      }
    }, error => {
      this.message.showNotification('danger', error.error.msg);
    });
  }

  getHomeData() {
    this.http.getData(ApiUrl.allVendors, {}).subscribe(res => {
      if (res.data) {
        // this.allVendors = res.data;
        // console.log(this.allVendors, '  this.allVendors');
        // this.vendorsCount = response.data.count;
      } else {
        this.message.showNotification('danger', res.msg);
      }
    }, error => {
      this.message.showNotification('danger', error.error.msg);
    });
  }

  blogData: any = [];

  getBlogData() {
    this.http.getData(ApiUrl.getBlogs, {limit:10}).subscribe(res => {
      this.blogData = res.data;
      this.setSlider('.blog-slider');
    });
  }

  feedbackData: any = [];

  getsitefeedback() {
    this.http.getData(ApiUrl.sitefeedback, {}).subscribe(res => {
      this.feedbackData = res.data;
      this.setSlider('.feed-slider');
    });
  }

  webHomeData: any = [];

  getWebHomeData() {
    this.http.getData(ApiUrl.webHomeData, {}).subscribe(res => {
      this.webHomeData = res.data;
    });
  }





  openBlog(data) {
    localStorage.setItem('blogData', JSON.stringify(data));
    this.router.navigate(['/blog-detail']);
  }

  openVendor(data) {
    localStorage.setItem('vendorData', JSON.stringify(data));
    this.router.navigate(['/vendor-detail']);
  }

}
