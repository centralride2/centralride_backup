$(".banner-wrapper").slick({
    autoplay: true,
    infinite: true,
    dots: false,
    slidesToShow: 1,
    slideswToScroll: 1,
    arrows: false
});


$(document).ready(function () {
    $(".booking-arrow").click(function () {
        $("#bookingdetail").toggle(500);
    });
});


$(document).ready(function ($) {
    $('.trips-slider').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        loop: true,
        arrows: true,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 767,
            settings: {
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });
});


$(document).ready(function ($) {
    $('.blog-slider').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        loop: true,
        arrows: true,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 1
            }
        }]
    });
});


$(document).ready(function ($) {
    $('.trending-slider').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        loop: true,
        arrows: true,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 1
            }
        }]
    });
});


// number function
(function () {
    'use strict';

    function ctrls() {
        var _this = this;

        this.counter = 0;
        this.els = {
            increment: document.querySelector('.ctrl-button-increment'),
            counter: {
                container: document.querySelector('.ctrl-counter'),
                num: document.querySelector('.ctrl-counter-num'),
                input: document.querySelector('.ctrl-counter-input')
            },
            decrement: document.querySelector('.ctrl-button-decrement')
        };

        this.decrement = function () {
            var counter = _this.getCounter();
            var nextCounter = (_this.counter > 0) ? --counter : counter;
            _this.setCounter(nextCounter);
        };

        this.increment = function () {
            var counter = _this.getCounter();
            var nextCounter = (counter < 9999999999) ? ++counter : counter;
            _this.setCounter(nextCounter);
        };

        this.getCounter = function () {
            return _this.counter;
        };

        this.setCounter = function (nextCounter) {
            _this.counter = nextCounter;
        };

        this.debounce = function (callback) {
            setTimeout(callback, 100);
        };

        this.render = function (hideClassName, visibleClassName) {
            _this.els.counter.num.classList.add(hideClassName);

            setTimeout(function () {
                _this.els.counter.num.innerText = _this.getCounter();
                _this.els.counter.input.value = _this.getCounter();
                _this.els.counter.num.classList.add(visibleClassName);
            },
                100);

            setTimeout(function () {
                _this.els.counter.num.classList.remove(hideClassName);
                _this.els.counter.num.classList.remove(visibleClassName);
            },
                200);
        };

        // this.ready = function () {
        //     _this.els.decrement.addEventListener('click',
        //         function () {
        //             _this.debounce(function () {
        //                 _this.decrement();
        //                 _this.render('is-decrement-hide', 'is-decrement-visible');
        //             });
        //         });

        //     _this.els.increment.addEventListener('click',
        //         function () {
        //             _this.debounce(function () {
        //                 _this.increment();
        //                 _this.render('is-increment-hide', 'is-increment-visible');
        //             });
        //         });

        //     _this.els.counter.input.addEventListener('input',
        //         function (e) {
        //             var parseValue = parseInt(e.target.value);
        //             if (!isNaN(parseValue) && parseValue >= 0) {
        //                 _this.setCounter(parseValue);
        //                 _this.render();
        //             }
        //         });

        //     _this.els.counter.input.addEventListener('focus',
        //         function (e) {
        //             _this.els.counter.container.classList.add('is-input');
        //         });

        //     _this.els.counter.input.addEventListener('blur',
        //         function (e) {
        //             _this.els.counter.container.classList.remove('is-input');
        //             _this.render();
        //         });
        // };
    };

    // init
    var controls = new ctrls();
    document.addEventListener('DOMContentLoaded', controls.ready);
})();
